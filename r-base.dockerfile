FROM rocker/r-ver:4.2.2

RUN apt-get update -qq && apt-get install -y \
    libssl-dev \
    libgdal-dev \
    libudunits2-dev \
    zlib1g-dev \
    libfontconfig1-dev

RUN R -e "install.packages('renv', repos = 'https://cloud.r-project.org')"


COPY renv.lock /app/renv.lock


# Adjust RENV_PATHS_CACHE to a directory within the project directory
ENV RENV_PATHS_CACHE=/app/renv/cache

# Set the working directory to /app
WORKDIR /app

COPY startR.sh /usr/local/bin/startR.sh
RUN chmod +x /usr/local/bin/startR.sh

ENTRYPOINT ["/usr/local/bin/startR.sh"]
