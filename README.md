# renv4docker
Kalonji 


## R Environment Docker Setup Documentation

## Prerequisites

Before proceeding with the setup, ensure that both Docker and R are installed on your computer. Docker will be used to containerise the R environment, while R is necessary for certain local operations and for understanding the context of R scripts and packages.

## Repository Contents

The repository contains the following key files:

- **`build_and_run_docker.sh`**: A shell script to build the Docker image from the Dockerfile and run a container from this image.
- **`r-base.dockerfile`**: The Dockerfile containing instructions for building the R environment image. It includes the installation of system dependencies, R packages via `renv`, and setup for the project workspace.
- **`renv.lock`**: A lock file generated by `renv` that ensures reproducibility by recording exact versions of R packages used in the project.
- **`run_docker.sh`**: A shell script to run a Docker container from the already built R environment image.
- **`startR.sh`**: A script executed at the container's startup. It's configured to kickstart the R environment or run an R script as the default container action.

## Setup Instructions

1. **Ensure Prerequisites**: Verify that Docker and R are installed on your system. Docker is essential for running the containerised R environment, and having R installed locally is useful for direct R script executions or package installations.

2. **Clone Repository**: Obtain a copy of this repository by cloning it from its source. This action will download the aforementioned files to your local machine.

3. **Build Docker Image**: Execute the `build_and_run_docker.sh` script. This script uses the `r-base.dockerfile` to build a Docker image of the R environment. The build process includes the installation of necessary system libraries and R packages as specified in the `r-base.dockerfile` and `renv.lock`.

   ```sh
   ./build_and_run_docker.sh

**Running the Docker Container**: After the image is built, it can be run using the run_docker.sh script. This action creates a container from the image, where the R environment is isolated and ready for use. The startR.sh script is automatically executed upon container startup, preparing the R environment for your project.

**Using the R Environment**: Once the container is running, you're in the configured R environment with all dependencies installed. You can now execute R scripts or start an R session within this isolated environment.