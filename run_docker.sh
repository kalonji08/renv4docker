#!/bin/bash

# Define image name
IMAGE_NAME="r-env-app"


docker run -it --rm \
    -v "$(pwd)":/app \
    -v "$(pwd)/renv/cache":/app/renv/cache \
    $IMAGE_NAME

