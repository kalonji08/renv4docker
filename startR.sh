#!/bin/bash

# Activate renv and restore the packages
R -e 'renv::restore()'
# Start an interactive R session
exec R
