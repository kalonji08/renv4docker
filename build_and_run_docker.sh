#!/bin/bash

# Define image name
IMAGE_NAME="r-env-app"

docker build -t $IMAGE_NAME -f r-base.dockerfile .

# Create a directory for RENV cache if it doesn't exist
mkdir -p ./renv/cache

docker run -it --rm \
    -v "$(pwd)":/app \
    -v "$(pwd)/renv/cache":/app/renv/cache \
    $IMAGE_NAME
